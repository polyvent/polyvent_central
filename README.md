# PolyVent Central Educational and Research V 2.0

This the top-level construction repo for the PolyVent V2.0, a completely open source ventilator designed for research and classroom education. 

For more information on PolyVent generally, please see the [Organisational Repo](https://gitlab.com/polyvent/organisational).

This repo specifically contains the latest Bill of Material and CAD files.

Other repos describe the software electronics, mixing module, valves module, etc. in greater detail.

This has the DOI [10.5281/zenodo.10642457](http://zenodo/10.5281/zenodo.10642457).

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10642457.svg)](https://doi.org/10.5281/zenodo.10642457)